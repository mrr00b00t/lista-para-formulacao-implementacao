# Variáveis de Decisão: Decidir quais vértices são escolhidos nas k cliques
# Objetivo: Maximizar a quantidade de vértices que estão na cobertura
# Sujeitos à:
# Não se pode escolher dois vértices que não exista uma aresta os ligando
# Ideia: Adaptar o código da clique máxima, fazendo a clique máxima k vezes, e adicionar mais um array
# de variáveis, que seria os vérticies que estão na cobertura. Ao inves de maximizar apenas um array
# de alguma clique em particular, maximizamos a soma de todas as variáveis para garantir que pegaremos
# a maior cobertura e a maior clique de cada array em particular


from docplex.mp.model import Model


model = Model('model')

N, E, K = [int(u) for u in input().split()]

Grafo = [[0 for j in range(N)] for i in range(N)]

for e in range(E):
    v, w = [int(u) for u in input().split()]
    Grafo[v-1][w-1] = 1
    Grafo[w-1][v-1] = 1

# Variáveis de Decisão: Decidir quais vértices estão na clique máxima
Variaveis = [
    [model.binary_var(name='Nó x{} está na clique K = {}?'.format(i+1,k)) for i in range(N)]
    for k in range(K)
]

# Variáveis da cobertura
VariaveisX = [model.binary_var(name='Nó x{} está na cobertura ? '.format(i+1)) for i in range(N)]

for i in range(N):
    model.add_constraint(VariaveisX[i] <= model.sum([Variaveis[j][i] for j in range(K)]))

# Sujeitos à:
# nao se pode escolher dois vértices que não exista uma aresta os ligando
for i in range(N):
    for j in range(N):
        if Grafo[i][j] != 1 and i != j:
            for k in range(K):
                model.add_constraint(
                    Variaveis[k][i] + Variaveis[k][j] <= 1
                )
                model.add_constraint(
                    Variaveis[k][i] + Variaveis[k][j] <= 1
                )

# Objetivo: Maximizar a quantidade de vértices que estão na cobertura e que estão
# em cada clique maximal que compõe a cobertura
model.maximize(
   model.sum(
       [VariaveisX[i] for i in range(N)]
   ) + model.sum(
       sum(
           [[Variaveis[k][i] for i in range(N)] for k in range(K)],
           []
       )
   )
)

print(model.export_to_string())

solution = model.solve(log_output=True)
solution.display()