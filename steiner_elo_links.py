# Variáveis de Decisão: Decidir a cor de cada aresta 
# Objetivo: Minimizar o custo do uso das cores, onde a cor i tem custo i
# Sujeitos à:
# Não se pode escolher a mesma cor para duas arestas adjacentes
# Ideia: uma matriz de inteiros, onde cada entrada representa a cor da aresta (i,j), se ela existir no grafo.
# Se nao existir, a cor é 0 (nenhuma cor). Maximizamos a soma da matriz, e dividimos por 2,
# pois e = (i,j) = (j,i).
# Foi usado um método para escrever uma diferença entre duas variáveis de forma linear, linhas 40-42.


from docplex.mp.model import Model
from itertools import combinations 


model = Model('model')

N, E, l, r = [int(u) for u in input().split()]
Grafo = [
    [0 for j in range(N)] for i in range(N)
]

ListaAdjacencia = [
    [] for i in range(N)
]

for e in range(E):
    v, w, c = [int(u) for u in input().split()]

    Grafo[v-1][w-1] = c
    Grafo[w-1][v-1] = c

    ListaAdjacencia[v-1].append((w-1, c))
    ListaAdjacencia[w-1].append((v-1, c))

TAM_T = int(input())

T = [int(u)-1 for u in input().split()]
V = [i for i in range(N)]

Variaveis = [
    [0 for j in range(N)] for i in range(N)
]

VariaveisSteiner = [model.binary_var(name='y{}'.format(i+1)) for i in range(N)]

VerticesSteiner = list(set(V) - set(T))

links = []
routers = []

for VS in VerticesSteiner:
    if len(ListaAdjacencia[VS]) == 2:
        links.append(VariaveisSteiner[VS])
    if len(ListaAdjacencia[VS]) >= 3:
        routers.append(VariaveisSteiner[VS])

aux_dict = {}
for i in range(N):
    for j in range(N):
        if i != j and Grafo[i][j] != 0:
            key_a = '{},{}'.format(i,j)
            key_b = '{},{}'.format(j,i) 

            if key_a not in aux_dict.keys() and key_b not in aux_dict.keys():
                aux_dict[key_a] = 1
                aux_dict[key_b] = 1

                nova_variavel = model.binary_var(name='x{}{} ou x{}{}'.format(i+1,j+1,j+1,i+1))
                Variaveis[i][j] = nova_variavel
                Variaveis[j][i] = nova_variavel

aux_dict = {}
for i in range(N):
    for j in range(N):
        if i != j and Grafo[i][j] != 0:
            key_a = '{},{}'.format(i,j)
            key_b = '{},{}'.format(j,i) 

            if key_a not in aux_dict.keys() and key_b not in aux_dict.keys():
                aux_dict[key_a] = 1
                aux_dict[key_b] = 1

                if i in VerticesSteiner:
                    model.add_constraint(
                        VariaveisSteiner[i] == Variaveis[i][j]
                    )
                else:
                    model.add_constraint(
                        VariaveisSteiner[i] == 1
                    )

                if j in VerticesSteiner:
                    model.add_constraint(
                        VariaveisSteiner[j] == Variaveis[i][j]
                    )
                else:
                    model.add_constraint(
                        VariaveisSteiner[j] == 1
                    )
                

for i in range(1, N+1):
    for combination in combinations(V, i):
        combination = list(combination)
        counter = 0

        for t in T:
            if t in combination:
                counter = counter + 1
        
        if counter != 0 and counter != TAM_T:
            corte = combination
            resto = list(set(V) - set(combination))

            para_somar = []

            for c in corte:
                for r in resto:
                    if Grafo[c][r] != 0:
                        para_somar.append(Variaveis[c][r])

            model.add_constraint(
                model.sum(
                    para_somar
                ) >= 1
            )

aux_dict = {}
variaveis_objetivo = []
custos_objetivo = []
for i in range(N):
    for j in range(N):
        if i != j and Grafo[i][j] != 0:
            key_a = '{},{}'.format(i,j)
            key_b = '{},{}'.format(j,i) 

            if key_a not in aux_dict.keys() and key_b not in aux_dict.keys():
                aux_dict[key_a] = 1
                aux_dict[key_b] = 1
                
                variaveis_objetivo.append(Variaveis[i][j])
                custos_objetivo.append(Grafo[i][j])

model.minimize(
    model.sum(
        [i*j for i,j in zip(variaveis_objetivo, custos_objetivo)]
    )
)

print(model.export_to_string())

solution = model.solve(log_output=True)
solution.display()