# Variáveis de Decisão: Decidir a cor de cada aresta 
# Objetivo: Minimizar o custo do uso das cores, onde a cor i tem custo i
# Sujeitos à:
# Não se pode escolher a mesma cor para duas arestas adjacentes
# Ideia: uma matriz de inteiros, onde cada entrada representa a cor da aresta (i,j), se ela existir no grafo.
# Se nao existir, a cor é 0 (nenhuma cor). Maximizamos a soma da matriz, e dividimos por 2,
# pois e = (i,j) = (j,i).
# Foi usado um método para escrever uma diferença entre duas variáveis de forma linear, linhas 40-42.


from docplex.mp.model import Model
from itertools import combinations 


model = Model('model')

N, E = [int(u) for u in input().split()]
ListaAdjacencia = [
    [] for i in range(N)
]

for e in range(E):
    v, w = [int(u) for u in input().split()]

    ListaAdjacencia[v-1].append(w-1)
    ListaAdjacencia[w-1].append(v-1)

# Variáveis de Decisão: Qual a cor de cada aresta
Variaveis = [
    [model.integer_var(name='Cor da aresta e{}{}'.format(i+1, j+1)) for j in range(N)] for i in range(N)
    ]

# Sujeitos à:
# A cor de cada aresta deve ser menor ou igual à quantidade máxima e maior que 0
# A cor da aresta i,j é igual a cor da aresta j,i
# Arestas vizinhas de um mesmo vértice não podem ter a mesma cor

for i in range(N):
    for p in list(combinations(ListaAdjacencia[i], 2)):
        aux_binary = model.binary_var(name='Binaria {},{},{}'.format(i,p[0],p[1]))
        model.add_constraint(Variaveis[i][p[0]] - Variaveis[i][p[1]] + aux_binary*E >= 1)
        model.add_constraint(Variaveis[i][p[0]] - Variaveis[i][p[1]] + aux_binary*E <= E - 1)

aux_dict = {}
for i in range(N):
    for j in range(N):
        if i != j:
            if j in ListaAdjacencia[i]:
                model.add_constraint(Variaveis[i][j] <= E)
                model.add_constraint(Variaveis[i][j] >= 1)
            else:
                model.add_constraint(Variaveis[i][j] == 0)

            if '{},{}'.format(i,j) not in aux_dict.keys() and '{},{}'.format(j,i) not in aux_dict.keys():
                aux_dict['{},{}'.format(i,j)] = True
                aux_dict['{},{}'.format(j,i)] = True
                
                model.add_constraint(Variaveis[i][j] == Variaveis[j][i])


# Objetivo: Minimizar o custo da coloração das arestas
# Obs.: dividindo por 2 para mostrar o resultado correto pois Variaveis[i][j] == Variaveis[j][i]
model.minimize(
   model.sum(
       sum(
           [[Variaveis[i][j] for j in range(N)] for i in range(N)],
           []
       )
   ) / 2
)

print(model.export_to_string())

solution = model.solve(log_output=True)
solution.display()